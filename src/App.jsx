import { useState } from "react"
import MiniSearch from 'minisearch'
import ReactDOM from 'react-dom/client'

function App() {

  // A collection of documents for our examples
  const documents = [
    {
      id: 1,
      title: 'Moby Dick',
      text: 'Call me Ishmael. Some years ago...',
      category: 'fiction'
    },
    {
      id: 2,
      title: 'Zen and the Art of Motorcycle Maintenance',
      text: 'I can see by my watch...',
      category: 'fiction',
      pathname: '#'
    },
    {
      id: 3,
      title: 'Neuromancer',
      text: 'The sky above the port was...',
      category: 'fiction'
    },
    {
      id: 4,
      title: 'Zen and the Art of Archery',
      text: 'At first sight it must seem...',
      category: 'non-fiction'
    },
    // ...and more
  ]

  let miniSearch = new MiniSearch({
    fields: ['title', 'text'], // fields to index for full-text search
    storeFields: ['title', 'category', 'pathname'] // fields to return with search results
  })

  // Index all documents
  miniSearch.addAll(documents)

  const setSearch = (e) => {

    if (e.target.value.length > 0) {
      let results = miniSearch.search(e.target.value);
      const list = document.createElement('ul');
      results.slice(0, 10).forEach(result => {
        const item = document.createElement('li');
        const link = document.createElement('a');
        link.setAttribute('href', result.pathname);
        link.appendChild(document.createTextNode(result.title));
        item.appendChild(link);
        list.append(item);
      });
      document.getElementById('results').replaceChildren(list);
    }
  }

  return (
    <div className="container">
      <h1>MiniSearch</h1>

      <form>
        <div className="form-group">
          <input type="text" className="form-control" id="searchbox" placeholder="Text to search" onChange={setSearch} />
          <small>"zen", "sky" </small>
        </div>
        <button type="submit" className="btn btn-primary">Submit</button>
      </form>

      <div id="results" className="container mt-5">
        <p>No results</p>
      </div>
    </div>
  )
}

export default App
