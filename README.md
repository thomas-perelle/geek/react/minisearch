# React Minisearch

This project is just a test of the Minisearch component to implement search capacities in a React application. 

To run the app locally:

```bash
npm install
npm run dev
```
